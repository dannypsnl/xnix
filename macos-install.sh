brew install \
  the_silver_searcher \
  wget \
  curl \
  unzip \
  ffmpeg \
  croc \
  graphviz \
  tree \
  cloc \
  tig \
  direnv \
  fzf
